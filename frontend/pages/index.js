import { useState } from 'react';
import { Row, Col } from 'react-simple-flex-grid';
import "react-simple-flex-grid/lib/main.css";
import styles from '../styles/Home.module.css'

export default function Home() {

  const [added, setAdded] = useState(false);

  function toggleAdded() {
    setAdded(!added);
  }

  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <button className={added ? styles.buttonAdded : styles.button} onClick={toggleAdded}>{added ? "Added to cart" : "Add to cart"}</button>
        
        <hr/>

        <Row gutter={30}>
          {Array.from(Array(15).keys()).map((value, index) => {
            return <Col key={index} xs={12} md={6} xl={3}>
              <div className={(index+1)%3 === 0 ? styles.blue : styles.green}></div>
            </Col>;
          })}
        </Row>
      </main>
    </div>
  )
}
