# Movie
Movie is a sample application which builds Node.js Restful CRUD APIs using Express and interacting with MySQL database by using Sequelize ORM.

## How to run locally
```bash
node server.js
```

## Functions
- API endpoints to create, read, update and delete pets from the service
- Capture the movie’s title, provider and price
- Swagger API documentation availble on `http://localhost:3000/api-docs`
- All paths under `/api` are protected via api keys
- Include testing using mocha and chai
- Service is run under Lambda
- Database is using MySQL
- Service is built using Node.js


## Documentation
Swagger API URL
`https://cbr43x29g2.execute-api.ap-southeast-2.amazonaws.com/dev/api-docs/`

## Export
`/api/movies` will return a list of the movies under both provider