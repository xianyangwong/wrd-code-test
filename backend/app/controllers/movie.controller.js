const db = require("../models");
const Movie = db.movies;
const Op = db.Sequelize.Op;

// Create and Save a new Movie
exports.create = (req, res) => {
    const { provider } = req.params

    // Validate request
    if (!req.body.title) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    // Create a Movie
    const movie = {
        title: req.body.title,
        provider: provider,
        price: req.body.price
    };

    // Save Movie in the database
    Movie.create(movie)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Movie."
            });
        });

};

// Retrieve all Movies from the database.
exports.findAll = (req, res) => {
    const { provider } = req.params
    var condition = { provider: { [Op.eq]: `${provider}` } };

    Movie.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving movies."
            });
        });

};

// Retrieve all Movies from the database.
exports.exportAll = (req, res) => {
    Movie.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving movies."
            });
        });

};

// Find a single Movie with an id
exports.findOne = (req, res) => {
    const { provider, id } = req.params
    var condition = {
        id: { [Op.eq]: id },
        provider: { [Op.eq]: `${provider}` }
    };

    Movie.findOne({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Movie with id=" + id
            });
        });
};

// Update a Movie by the id in the request
exports.update = (req, res) => {
    const { id, provider } = req.params;

    Movie.update(req.body, {
        where: { id: id, provider: provider }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Movie was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Movie with id=${id}. Maybe Movie was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Movie with id=" + id
            });
        });

};

// Delete a Movie with the specified id in the request
exports.delete = (req, res) => {
    const { id, provider } = req.params;

    Movie.destroy({
        where: { id: id, provider: provider }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Movie was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Movie with id=${id}. Maybe Movie was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Movie with id=" + id
            });
        });
};

// Delete all Movies from the database.
exports.deleteAll = (req, res) => {
    const { id, provider } = req.params;

    Movie.destroy({
        where: { provider: provider },
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Movies were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all movies."
            });
        });
};

// Find all published Movies
exports.findAllPublished = (req, res) => {
    Movie.findAll({ where: { published: true } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving movies."
            });
        });
};