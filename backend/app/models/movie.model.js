module.exports = (sequelize, Sequelize) => {
    const Movie = sequelize.define("movie", {
      title: {
        type: Sequelize.STRING
      },
      provider: {
        type: Sequelize.ENUM('cinemaworld', 'firmworld'),
        validate: {
            isIn: [['cinemaworld', 'firmworld']]
        }
      },
      price: {
        type: Sequelize.DECIMAL(10,2),
        validate: {
            isDecimal: true
        }
      }
    });
  
    return Movie;
  };