module.exports = {
    HOST: 'db4free.net',
    USER: "movie_user",
    PASSWORD: "password",
    DB: "movie_codetest",
    dialect: process.env.NODE_ENV === 'test' ? 'sqlite' : 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};