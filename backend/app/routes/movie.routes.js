module.exports = app => {
    const movies = require("../controllers/movie.controller.js");
  
    var router = require("express").Router();
  
    /**
     * @openapi
     * /api/:provider/movies:
     *   post:
    *     description: Create a single movie
    *     responses:
    *       200:
    *         description: Returns a created movie.
    */
    router.post("/:provider/movies/", movies.create);
  
    /**
     * @openapi
     * /api/:provider/movies:
     *   get:
    *     description: Retrieve all movies under a provider
    *     responses:
    *       200:
    *         description: Returns a list movies under provider.
    */
    router.get("/:provider/movies/", movies.findAll);
  
    /**
     * @openapi
     * /api/:provider/movie/:id:
     *   get:
    *     description: Retrieve movie under a provider by id
    *     responses:
    *       200:
    *         description: Returns a movie under provider by id
    */
    router.get("/:provider/movie/:id", movies.findOne);
  
    /**
     * @openapi
     * /api/:provider/movie/:id:
     *   put:
    *     description: Update movie under a provider by id
    *     responses:
    *       200:
    *         description: Returns a updated movie under provider by id
    */
    router.put("/:provider/movie/:id", movies.update);
  
    /**
     * @openapi
     * /api/:provider/movie/:id:
     *   delete:
    *     description: Delete movie under a provider by id
    *     responses:
    *       200:
    *         description: Message Movie was deleted successfully!
    */
    router.delete("/:provider/movie/:id", movies.delete);
  
    /**
     * @openapi
     * /api/:provider/movies:
     *   delete:
    *     description: Delete all movies under provider
    *     responses:
    *       200:
    *         description: ${nums} Movies were deleted successfully!
    */
    router.delete("/:provider/movies/", movies.deleteAll);

    /**
     * @openapi
     * /api/movies:
     *   get:
    *     description: Retrieve all movies 
    *     responses:
    *       200:
    *         description: Returns a list movies.
    */
    router.get("/movies/", movies.exportAll);
  
    app.use('/api', router);
  };