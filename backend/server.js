const express = require("express");
const cors = require("cors");
const db = require("./app/models");
const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');
const bodyParser = require('body-parser')

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Movie api',
      version: '1.0.0',
    },
  },
  apis: ['./app/routes/*.routes.js'], // files containing annotations as above
};

const openapiSpecification = swaggerJsdoc(options);

const app = express();

app.use(cors());

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({extended: true}));

require("./app/routes/movie.routes")(app);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openapiSpecification));


if (process.env.NODE_ENV === 'test') {
  // drop the table if it already exists
  db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
  });

} else {
  db.sequelize.sync();
}


app.get("/", (req, res) => {
  res.json({ message: "Welcome to movie application." });
});

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

module.exports = app;