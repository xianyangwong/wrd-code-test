process.env.NODE_ENV = "test"

const chai = require('chai')
const chaiHttp = require('chai-http')
var expect = require('chai').expect;
chai.use(chaiHttp)

let server = require('../server')

describe('Movie api', function () {

   it('GET /', (done) => {
      chai.request(server)
         .get('/')
         .end((err, res) => {
            expect(res.body.message).to.be.equal('Welcome to bezkoder application.')
            done()
         })
   })

   it('POST /api/cinemaworld/movies', (done) => {
      chai.request(server)
         .post('/api/cinemaworld/movies')
         .send({
            "title": "test",
            "price": 10
        })
         .end((err, res) => {
            expect(res.body.title).to.be.equal('test')
            expect(res.body.price).to.be.equal(10)
            done()
         })
   })

   it('GET /api/cinemaworld/movie/1', (done) => {
      chai.request(server)
         .get('/api/cinemaworld/movie/1')
         .end((err, res) => {
            expect(res.body.title).to.be.equal('test')
            expect(res.body.price).to.be.equal(10)
            done()
         })
   })

   it('GET /api/cinemaworld/movies', (done) => {
      chai.request(server)
         .get('/api/cinemaworld/movies')
         .end((err, res) => {
            expect(res.body.length).to.be.equal(1)
            done()
         })
   })

   // it('POST /api/tutorial', (done) => {
   //    chai.request(server)
   //       .post('/api/tutorials')
   //       .send({
   //          title: 'test title',
   //          description: 'test desc',
   //          publised: false
   //       })
   //       .end((err, res) => {
   //          expect(res.body.title).to.be.equal('test title')
   //          done()
   //       })
   // })

   // it('GET /api/tutorial', (done) => {
   //    chai.request(server)
   //       .get('/api/tutorials')
   //       .end((err, res) => {
   //          expect(res.body.length).to.be.equal(1)
   //          done()
   //       })
   // })

});